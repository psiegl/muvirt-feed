#!/usr/bin/env lua

local app = require "pl.app"
local utils = require "pl.utils"

local appstore = require "appstore"

function print_usage()
    print("baremetal-deploy - deploy an appliance from the appliance store to a bare metal storage device\n")
    print("Usage: baremetal-deploy [options] appliance-id device")
    print("Options:")
    print("\t --store=APPSTORE \t search only in this appliance store")
    print("\t --rootpw=ROOTPW  \t set the root password on the written disk")
    print("\t --ignore-incompat\t ignore any listed incompatibilites with the current hardware")
end

function download_image(downloadURL)
    local curl_command = "curl " .. downloadURL .. " -L -o /tmp/deployimage.bin"
    local download_success, curl_return, curl_stdout, curl_stderr  = utils.executeex(curl_command, false)

    if (download_success ~= true) then
        print("ERROR: Failed to download image, output was:")
        print(curl_errout)
        return false
    end
    return true
end

function write_image(format, device)
    if (format ~= "qcow2") then
        if (format == "raw-gz") then
            os.rename("/tmp/deployimage.bin","/tmp/deployimage.bin.gz")
            local gzip_success, gzip_return, gzip_stdout, gzip_stderr = utils.executeex("gunzip /tmp/deployimage.bin.gz")
            if (gzip_success ~= true) then
                print("ERROR: Failed to decompress gzip image")
                return false
            end
        elseif (format == "raw-xz") then
            os.rename("/tmp/deployimage.bin","/tmp/deployimage.bin.xz")
            local unxz_success, unxz_return, unxz_stdout, unxz_stderr = utils.executeex("unxz /tmp/deployimage.bin.xz")
        end
    end
    local write_command = "write-image-disk /tmp/deployimage.bin " .. device
    local write_success, write_return, write_stdout, write_stderr = utils.executeex(write_command, false)
    if (write_success ~= true) then
        print("ERROR: Failed to write image to device")
        print(write_stderr)
        print(write_stdout)
        return false
    end
    return true
end

local flags, params = app.parse_args(nil,
    { "store","rootpw"},  -- list of flags taking values
     {"help","ignore-incompat"})      -- list of allowed flags (value ones will be added)

if (type(flags) ~= "table") then
    print("ERROR parsing command line arguments: ")
    print(flags)
end

if (type(flags) ~= "table") or (flags["help"] ~= nil and flags["help"] == true) or (#params < 2) then
    print_usage()
    os.exit(1)
end

local store = nil
local appliance = params[1]
local device = params[2]
local rootpw = nil

local listpath = appstore.getAppStoreCachePath()
local haveDownloadedLists = appstore.haveAppStoreLists(listpath)
if (haveDownloadedLists ~= true) then
    print("Lists not downloaded, will attempt to download")
    local downloadSuccessful = appstore.downloadAppStoreLists()
    if (downloadSuccessful ~= true) then
        print ("ERROR: Could not download appliance store lists, please check your internet connectivity")
        os.exit(1)
    end
end

local applianceInfo = appstore.searchForAppliance(appliance)
if (applianceInfo == nil) then
    print("ERROR: Appliance " .. appliance .. " not found in downloaded lists")
    os.exit(1)
end

--- Check appliance compatibility with the hardware
print("Checking compatibility... ")
ourHardwareIDs = appstore.getCurrentHardwareIDs()
local isCompatible = appstore.checkCompatibility(applianceInfo, ourHardwareIDs)

if (isCompatible ~= true) then
    if (flags["ignore-incompat"] ~= true) then
        print ("ERROR: Appliance " .. appliance .. " is not compatible with this hardware, you can use --ignore-incompat to override")
        os.exit(1)
    else
        print ("WARNING: Appliance " .. appliance .. " is not compatible with this hardware, continuing on as --ignore-incompat was specified")
    end
end

local applianceName = applianceInfo["name"]
print("\n")
print("Appliance to deploy: " .. appliance .. " - " .. applianceName)
print("Device to deploy to: " .. device)

print("Downloading image")
local downloadURL = applianceInfo["download"]
local downloadSuccess = download_image(downloadURL)
if (downloadSuccess ~= true) then
    print("ERROR: Could not download image from " .. downloadURL .. ", exiting")
    os.exit(1)
end

print("\n")
print("! ! ! ! ! ! ! ! ! !")
print("About to write image to device " .. device .. " if you don't want this, Ctrl-C now!")
print("! ! ! ! ! ! ! ! ! !")
local start = os.clock()
while os.clock() - start < 10 do end

local format = applianceInfo["format"] or "qcow2"

local writeSuccess = write_image(format, device)
if (writeSuccess ~= true) then
    os.exit(1)
end
