#!/usr/bin/lua

local appstore = require "appstore"

local app = require "pl.app"

is_baremetal, mregistry = pcall(require, "manipulatorregistry")

function print_heading()
    local heading = string.format("%-16s|%-32s|%-32s","registry","appliance id","description")
    print(heading)
    local delineator = string.format("%-16s|%-32s|%-32s","","","")
    delineator = delineator:gsub(" ", "-")
    print(delineator)
end

function printAppliancesInStore(storename, storelist)
    local hiddenAppliances = 0
    for appidx in pairs(storelist) do
        local appinfo = storelist[appidx]
        local appid = appinfo["id"]
        local compatible = true
        local hasfixup = false
        local fixupflag = ""
        if (showallappliances == false) then
            compatible = appstore.checkCompatibility(appinfo, ourhardwareIDs)
            if (compatible ~= true and is_baremetal == true) then
                local fixups_for_appliance = mregistry.find_manipulator_for_appliance(appid)
                hasfixup = (#fixups_for_appliance > 0)
                fixupflag="*"
            end
        end

        if (compatible ~= true and hasfixup == false) then
            hiddenAppliances = hiddenAppliances+1
        else
            local appname = appinfo["name"]
            local paddedprint = string.format("%-16s|%-31s%1s|%-32s", storename, appid, fixupflag, appname)
            print(paddedprint)
        end
    end
    return hiddenAppliances
end

function print_usage()
    print("appstore-list - list appliances available in the appliance store")
    print("Options: ")
    print("--store=STORE\tShow this store only")
    print("--show-all\tShow all appliances in the store registry, even if they are not compatible with the currrent platform")
end

local flags, params = app.parse_args(nil,
    { "store"},  -- list of flags taking values
     {"help","show-all"})      -- list of allowed flags (value ones will be added)

if (type(flags) ~= "table") then
    print("ERROR parsing command line arguments: ")
    print(flags)
end

if (type(flags) ~= "table") or (flags["help"] ~= nil and flags["help"] == true) then
    print_usage()
    os.exit(1)
end

cachepath = appstore.getAppStoreCachePath()
havelists = appstore.haveAppStoreLists(cachepath)
if (havelists ~= true) then
    print("Lists not downloaded, will attempt to download")
    local downloadSuccessful = appstore.downloadAppStoreLists()
    if (downloadSuccessful ~= true) then
        echo ("ERROR: Could not download appliance store lists, please check your internet connectivity")
        os.exit(1)
    end
end

if (is_baremetal == true) then
	local loaded_manipulators, errorstr = pcall(mregistry.load_manipulators)
    if (loaded_manipulators ~= true) then
        print("WARNING: Could not load manipulator list: " .. errorstr)
    end
end
appstorelist = appstore.assembleAppStoreApplianceList(cachepath)
showallappliances = flags["show-all"] or false

showonly = flags["store"] or nil

ourhardwareIDs = appstore.getCurrentHardwareIDs()
print_heading()

local wrong_schema_repos = {}

local total_hidden_appliances = 0
for reponame,repodata in pairs(appstorelist) do
    if (showonly == nil) or (showonly == reponame) then
        local is_schema_out_of_date = appstore.isRepositoryAtSchemaLevel(reponame)
        if (is_schema_out_of_date ~= true) then
            wrong_schema_repos[#wrong_schema_repos+1] = reponame
        end
        local this_hidden_appliances = printAppliancesInStore(reponame,repodata)
        total_hidden_appliances = total_hidden_appliances + this_hidden_appliances
    end
end
if (total_hidden_appliances >= 1) then
    print("[" .. total_hidden_appliances .. " entries hidden due to incompatibility with this platform" .. ", use --show-all to display them]")
end

local is_baremetal = pcall(require,"manipulatorregistry")
if (is_baremetal) then
    print("* = uses fixup manipulator to work on this platform")
    print("P = provisioner manipulator available")
end

if (#wrong_schema_repos >= 1) then
    print("WARNING: The following appliance store lists are in a newer schema version:")
    for i,reponame in pairs(wrong_schema_repos) do
        print("\t"..reponame)
    end
    print("Please upgrade your system to ensure compatibility")
end