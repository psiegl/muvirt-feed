local uci = require "luci.model.uci".cursor()
local lfs = require "lfs"

local appstore = {}

appstore.implementedSchemaLevel = 2

function appstore.getAppStoreCachePath()
  cachepath = nil
  uci:foreach("appstore","appstore", function(s)
      name = s[".name"]
      if (s["listcache"] ~= nil and cachepath == nil) then
        cachepath = s["listcache"]
      end
    end)
    if (cachepath == nil) then
      cachepath = "/tmp/appstorecache/"
    end
  return cachepath
end

function appstore.getRepositoryFile(repository)
  local cachepath = appstore.getAppStoreCachePath()
  local f = io.open(cachepath .. repository)
  local s = f:read("*a")
  f:close()
  data = luci.jsonc.parse(s)
  return data
end

function appstore.getSchemaLevel(repository)
  local repositorydata = appstore.getRepositoryFile(repository)
  if (repositorydata ~= nil) then
    if (repositorydata["schemaLevel"] ~= nil) then
      return repositorydata["schemaLevel"]
    end
  end
  return nil
end

function appstore.isRepositoryAtSchemaLevel(repository)
  local repositorySchemaLevel = appstore.getSchemaLevel(repository)
  if (repositorySchemaLevel == nil) then
    error("Repository " .. repository .. " not found")
  end
  if (repositorySchemaLevel ~= appstore.implementedSchemaLevel) then
    return false
  end
  return true
end

function appstore.haveAppStoreLists(cachepath)
  local hasdirectory = lfs.attributes(cachepath, 'mode')
  if (hasdirectory ~= 'directory') then
    return false
  end
  for storefile in lfs.dir(cachepath) do
    if (lfs.attributes(cachepath .. "/" .. storefile, 'mode')) == 'file' then
      return true
    end
  end
  return false
end

function appstore.downloadAppStoreLists()
  local downloadprog = io.popen("appstore-download","r")
  local downloadoutput = downloadprog:read('*all')
  local downloadreturn = {downloadprog:close()}
  if (downloadreturn[1] == true) then
    return true
  else
    print ("Could not download lists, got output: ")
    print(downloadoutput)
  end
  return false
end

function appstore.assembleAppStoreApplianceList(cachepath)
  masterlist = {}
  for file in lfs.dir(cachepath) do
    if lfs.attributes(file, 'mode') ~='directory' then
      listdata = appstore.getRepositoryFile(file)
      if (listdata ~= nil and listdata["appliances"] ~= nil) then
      	masterlist[file] = listdata["appliances"]
      end
    end
  end
  return masterlist
end

function appstore.assembleAppStoreApplianceListCompatibleOnly(cachepath)
  masterlist = {}
  hwids = appstore.getCurrentHardwareIDs()
  for file in lfs.dir(cachepath) do
    if lfs.attributes(file, 'mode') ~='directory' then
      listdata = appstore.getRepositoryFile(file)
      if (listdata ~= nil and listdata["appliances"] ~= nil) then
        this_list_appliances = listdata["appliances"]
        compatible_appliances = {}

        for appidx in pairs(this_list_appliances) do
          appliancedata = this_list_appliances[appidx]
          local compatible = appstore.checkCompatibility(appliancedata, hwids)
          if (compatible ~= false) then
            compatible_appliances[#compatible_appliances+1] = appliancedata
          end
        end

        masterlist[file] = compatible_appliances
      end
    end
  end
  return masterlist
end

function appstore.getApplianceInfo(repository, applianceID)
  local repoFile = appstore.getRepositoryFile(repository)
  if (repoFile == nil) then
    return nil
  end
  local repoAppliances = repoFile["appliances"]
  if (repoAppliances == nil) then
    return nil
  end
  for i,v in ipairs(repoAppliances) do
    if (v["id"] ~= nil and v["id"] == applianceID) then
      return v
    end
  end
  return nil
end

function appstore.searchForAppliance(applianceID)
  local cachepath = appstore.getAppStoreCachePath()
  for file in lfs.dir(cachepath) do
    if lfs.attributes(cachepath .. "/" .. file, 'mode') == 'file' then
      local hasAppliance = appstore.getApplianceInfo(file, applianceID)
      if (hasAppliance ~= nil) then
        return hasAppliance
      end
    end
  end
  return nil
end

function appstore.findStoreForAppliance(applianceID)
  local cachepath = appstore.getAppStoreCachePath()
  for storefile in lfs.dir(cachepath) do
    if lfs.attributes(cachepath .. "/" .. storefile, 'mode') == 'file' then
      local hasAppliance = appstore.getApplianceInfo(storefile, applianceID)
      if (hasAppliance) then
        return storefile
      end
    end
  end
  return nil
end

function appstore.getCurrentHardwareIDs(ignorevirt)
  local hardwareids = {}
  ignorevirt = ignorevirt or false
  if (ignorevirt ~= true) then
    local hasmuvirt = pcall(require,"muvirt")
    if (hasmuvirt == true) then
      hardwareids[1] = "virt"
      return hardwareids
    end
  end

  -- For test purposes, allow the compatible to be overridden by
  -- kernel commandline
  local cmdlinefile = io.open("/proc/cmdline","r")
  if (cmdlinefile ~= nil) then
    local cmdline = cmdlinefile:read("*a")
    io.close(cmdlinefile)
    local appstore_compat_iterator = cmdline:gmatch("appstore\.compatible=([%w,]+)")
    for compat in appstore_compat_iterator do
      hardwareids[1] = compat
      return hardwareids
    end
  end

  local compatfile = io.open("/sys/firmware/devicetree/base/compatible","r")
  if (compatfile == nil) then
    return hardwareids
  end

  --- The compatible file readout is delimited by the NUL byte (\0)
  --- which is frustrating to deal with when using string functions.
  --- But we can use this fact to our advantage - just seek past each NUL
  --- until there is no more
  local compatsize = compatfile:seek("end")
  local curptr = 0
  while (curptr < compatsize) do
    compatfile:seek("set",curptr)
    thishwid = compatfile:read("*a")
    thishwlen = thishwid:find("\0")
    thishwid = thishwid:sub(0,thishwlen-1)
    hardwareids[#hardwareids+1] = thishwid
    curptr = curptr + thishwlen
  end
  io.close(compatfile)
  return hardwareids
end

function appstore.checkCompatibility(applianceMetadata, hwIDs)
  hwIDs = hwIDs or {"virt"}

  --- Supported hardware is an explict allow list, if the hardware
  --- is not in it, then it is not compatible
  local supported_hardware = applianceMetadata["supported_hardware"]
  
  if (supported_hardware ~= nil) and (#supported_hardware > 0) then
    for idx,supported_hwid in pairs(supported_hardware) do
      for x,hwID in pairs(hwIDs) do
        if (hwID == supported_hwid) then
          return true
        end
      end
    end
    return false
  end

  local incompat_hardware = applianceMetadata["incompat_hardware"]
  if (incompat_hardware ~= nil) and (#incompat_hardware > 0) then
    for idx,incompat_hw in pairs(incompat_hardware) do
      for x,hwID in pairs(hwIDs) do
        if (hwID == incompat_hw) then
          return false
        end
      end
    end
  end
  return true
end

function appstore.create_library_identifier(applianceSource, applianceID, url)
  return applianceSource .."-"..applianceID.."|"..url
end

function appstore.deployAppliance(newSectionID, applianceSource, applianceID, configArguments)
  local returnData = {}
  local appliance = appstore.getApplianceInfo(applianceSource, applianceID)
  if (appliance == nil) then
    returnData["error"] = "appliance " .. applianceSource .. "/" .. applianceID .. " not found"
    return returnData
  end

  local hasmuvirt = pcall(require,"muvirt")
  if (hasmuvirt == false) then
    returnData["error"] = "muvirt is not present on this system"
    return returnData
  end

  local newVMConfig = configArguments
  local librarypath = appstore.create_library_identifier(applianceSource, applianceID, appliance["download"])
  newVMConfig["imageurl"] = librarypath
  newVMConfig["checksum"] = appliance["checksum"]

  if (appliance["format"] ~= nil) then
  	newVMConfig["imageformat"] = appliance["format"]
  end

  newVMConfig["provisioned"] = "0"
  newVMConfig["enable"] = "1"
  newVMConfig["mac"] = muvirt_generate_random_mac()
  newVMConfig["network"] = {muvirt_get_default_network()}

  if (appliance["cloudinit"] == false) then
    newVMConfig["cloudinit"] = "0"
  end

  if (newVMConfig["numprocs"] == nil) then
    if (appliance["mincpu"] ~= nil) then
      newVMConfig["numprocs"] = appliance["mincpu"]
    else
      newVMConfig["numprocs"] = "1"
    end
  end

  if (newVMConfig["memory"] == nil) then
    if (appliance["minram"] ~= nil) then
      newVMConfig["memory"] = appliance["minram"]
    else
      newVMConfig["memory"] = "512"
    end
  end

  if (newVMConfig["disksize"] == nil) then
    if (appliance["mindisk"] ~= nil) then
      newVMConfig["disksize"] = appliance["mindisk"]
    else
      newVMConfig["disksize"] = "4096"
    end
  end

  local saved = uci:section("virt","vm",newSectionID, newVMConfig)
  if (saved == false) then
    returnData["error"] = "appliance " .. newVMID .. " could not be saved, possibly due to invalid characters in VM ID"
    return returnData
  end

  uci:save("virt")
  uci.apply()

  newVMConfig["name"] = newSectionID
  return newVMConfig
end

return appstore