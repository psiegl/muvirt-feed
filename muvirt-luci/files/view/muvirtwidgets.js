'use strict';
'require ui';
'require rpc';
'require form';
'require uci';
'require tools.widgets as widgets';
'require network';

function progressbar(value, max, byte) {
	var vn = parseInt(value) || 0,
	    mn = parseInt(max) || 100,
	    fv = byte ? String.format('%1024.2mB', value) : value,
	    fm = byte ? String.format('%1024.2mB', max) : max,
	    pc = Math.floor((100 / mn) * vn);

	return E('div', {
		'class': 'cbi-progressbar',
		'title': '%s / %s (%d%%)'.format(fv, fm, pc)
	}, E('div', { 'style': 'width:%.2f%%'.format(pc) }));
}

var getStorageOverview = rpc.declare({
  object: 'muvirtstorage',
  method: 'overview',
  params: ['device']
});

var listUSBDevices = rpc.declare({
  object: 'muvirt',
  method: 'listusbdevs',
  params: [],
});

var OrderedNetworkSelectWidget = form.ListValue.extend({
  __name__: 'OrderedNetworkSelect',

  load: function(section_id) {
    return network.getNetworks().then(L.bind(function(networks) {
			this.configuredNetworks = networks;

			return this.super('load', section_id);
		}, this));

  },
  renderWidget: function(section_id, option_index, cfgvalue) {
    var sectionCBID = this.cbid(section_id);

    var table = E('table', {'id': sectionCBID});
    var tbody = E('tbody', {"id": sectionCBID+"_tbody"});

    table.appendChild(tbody);

    this.valid_networks = {};

    //var value = (cfgvalue != null) ? cfgvalue : this.default,
    //hiddenEl = new ui.Hiddenfield(value, { id: sectionCBID });

    this.dropdowns = [];


    for (var j = 0; j < this.configuredNetworks.length; j++) {
      var configuredNetwork = this.configuredNetworks[j];
      var underlyingDevice = configuredNetwork.getDevice();
      if (underlyingDevice.isBridge())
        this.valid_networks[configuredNetwork.getName()] = configuredNetwork.getName();
    }

    if (Array.isArray(cfgvalue)) {
      for(var i=0; i<cfgvalue.length; i++) {
        var row = this.createInterfaceEntry(sectionCBID, cfgvalue[i]);
        tbody.appendChild(row);
      }
    }
    var self = this;
    var addButton = E('button', {'class': 'cbi-button'}, _('Add another interface'));
    addButton.onclick = function(event) {
      self.addAnotherInterface(sectionCBID, tbody.id);
    };
    this.node = table;
    L.dom.bindClassInstance(table, this);

    return E([table, addButton]);
  },
  createInterfaceEntry: function(basecbid, value) {
    var thisRow = E('tr');
    var thisCell = E('td');

    var thisRowId = this.dropdowns.length;
    var dropdown = new ui.Dropdown(value,this.valid_networks, {
      id: basecbid+"_"+thisRowId,
      sort: true,
      multiple: false,
      create: false
    });
    this.dropdowns.push(dropdown);
    thisCell.appendChild(dropdown.render());

    thisRow.appendChild(thisCell);

    var maccell = E('td');
    var macoverride = E('input', {'type': 'text', 'placeholder': _('Override MAC address')});

    maccell.appendChild(macoverride);

    thisRow.appendChild(maccell);

    var deleteCell = E('td');
    var deleteButton = E('button', {'class': 'cbi-button'}, _("Remove"));

    deleteButton.onclick = L.ui.createHandlerFn(this, 'removeRow', dropdown);

    deleteCell.appendChild(deleteButton);
    thisRow.appendChild(deleteCell);

    return thisRow;
  },
  addAnotherInterface: function(section_id, tbodyid) {
    console.log("(this.)add another interface for "+section_id);
    var tbody = document.getElementById(tbodyid);

    var newEntry = this.createInterfaceEntry(section_id, null);
    tbody.appendChild(newEntry);
  },
  textvalue: function(section_id) {
    console.log("ordered network select textvalue: "+section_id);
  },
  formvalue: function(section_id) {
    console.log("ordered network select formvalue: "+section_id);
    return this.getValue();
  },
  getValue: function() {
    console.log("ordered network select getvalue");
    var values = [];

    for(var i=0; i<this.dropdowns.length; i++) {
      var dd = this.dropdowns[i];
      var value = dd.getValue();
      values.push(value);
    }
    return values;
  },
  triggerValidation: function(section_id) {
    console.log("ordered network select trigger validation: " + section_id);
    if (this.dropdowns == null)
      return false;

    for(var i=0; i<this.dropdowns.length; i++) {
      var dd = this.dropdowns[i];
      var value = dd.getValue();
      if (value == null) {
        return false;
      }
    }
    console.log("ordered network select validation returned");
    return true;
  },
  isValid: function(section_id) {
    for(var i=0; i<this.dropdowns.length; i++) {
      var dd = this.dropdowns[i];
      var value = dd.getValue();
      if (value == null) {
        return false;
      }
    }

    console.log("ordered entwork select is valid: "+section_id);
    return true;
  },
  removeRow: function(dropdown, event) {
    var targetButton = event.target;
    var targetParentCell = targetButton.parentNode;
    var targetParentRow = targetParentCell.parentNode;

    this.dropdowns.splice(this.dropdowns.indexOf(dropdown,1));

    var targetParentTable = targetParentRow.parentNode;
    targetParentTable.removeChild(targetParentRow);
  }
});

var DiskSelectWidget = form.Value.extend({
  __name__: "DiskSelect",
  load: function(section_id) {
    this.diskRows = [];
    return this.super('load', section_id);
  },
  renderWidget: function(section_id, option_index, cfgvalue) {
    var sectionCBID = this.cbid(section_id);

    var table = E('table', {'class': 'table', 'id': sectionCBID});
    var thead = E('thead', {}, [
      E('tr', {'class': 'tr cbi-section-table-titles'}, [
        E('th', {'class': 'th cbi-section-table-cell'}, _('Storage device')),
        E('th', {'class': 'th cbi-section-table-cell'}, _('Device serial')),
        E('th', {'class': 'th cbi-section-table-cell'}, _('Remove'))
      ]),
    ]);
    table.appendChild(thead);
    var tbody = E('tbody', {"id": sectionCBID+"_tbody"});

    table.appendChild(tbody);
    var addExistingButton = E('button', {'class': 'cbi-button'}, _('Add existing storage device'));
    addExistingButton.onclick = L.ui.createHandlerFn(this, 'handleAddExistingStorageDevice');

    var addNewButton = E('button', {'class': 'cbi-button'}, _('Add new storage device (not yet implemented)'));
    addNewButton.onclick = null;
    this.node = table;
    this.tbody = tbody;
    L.dom.bindClassInstance(table, this);

    if (Array.isArray(cfgvalue)) {
      for(var i=0; i<cfgvalue.length; i++) {
        var row = this.handleAddExistingStorageDevice(cfgvalue[i]);
      }
    }

    return E([table, addExistingButton, addNewButton]);
  },

  handleAddExistingStorageDevice: function(arg) {
    var input = E('input', {'name': 'disk',
      'type': 'text', 'placeholder': _('Storage device')});
    var inputCell = E('td', {'class': 'td cbi-section-table-cell'}, input);

    var deviceSerialInput = E('input', {'name': 'diskserial', 'type': 'text'});

    var deviceSerialCell = E('td',
      {'class': 'td cbi-section-table-cell', 'placeholder': _('Optional')},
      deviceSerialInput);

    if (typeof arg == "string") {
        var diskArguments = this.parseDiskString(arg);
        if ("disk" in diskArguments) {
          input.value = diskArguments["disk"];
        }
        if ("serial" in diskArguments) {
          deviceSerialInput.value = diskArguments["serial"];
        }
    }

    var removeButton = E('button', {'class': 'cbi-button'}, _('Remove'));
    removeButton.onclick = L.ui.createHandlerFn(this, 'handleRemoveStorageDevice');
    var removeCell = E('td', {'class': 'td cbi-section-table-cell'}, removeButton);
    var row = E('tr', {}, [inputCell, deviceSerialCell, removeCell]);
    this.diskRows.push(row);
    this.tbody.appendChild(row);
  },
  handleRemoveStorageDevice: function(event) {
    var target = event.target;
    var targetParentRow = target.parentNode.parentNode; // td up t tr
    var targetParentTbody = targetParentRow.parentNode;
    this.diskRows.splice(this.diskRows.indexOf(targetParentRow),1);
    targetParentTbody.removeChild(targetParentRow);
  },
  validate: function(section_id, value) {
    console.log("disk validate: "+section_id+"="+value);
  },
  isValid: function(section_id) {
    for(var i=0; i<this.diskRows.length; i++) {
      var pathField = this.diskRows[i].querySelector("input[name=disk]");
      if (pathField.value.length == 0)
        return false;
    }
    return true;
  },
  textvalue: function(section_id) {
    console.log("disk select textvalue: "+section_id);
  },
  formvalue: function(section_id) {
    console.log("disk select formvalue: "+section_id);
    var values = [];
    for(var i=0; i<this.diskRows.length; i++) {
      var pathField = this.diskRows[i].querySelector("input[name=disk]");
      if (pathField.value.length == 0)
        continue;
      var uciValue = this.generateDiskString(this.diskRows[i]);
      values.push(uciValue);
    }
    return values;
  },
  getValue: function() {
    console.log("disk select getvalue");
    var values = [];

    return values;
  },
  triggerValidation: function(section_id) {
    console.log("disk select trigger validation: " + section_id);
    if (this.diskRows == null)
      return;

    for(var i=0; i<this.diskRows.length; i++) {
      var pathField = this.diskRows[i].querySelector("input[name=disk]");
      if (pathField.value.length == 0)
        return false;
    }
    return true;
  },
  parseDiskString: function(value) {
    var returnValues = {};
    var values = value.split(",");
    returnValues["disk"] = values[0];
    if (values.length > 1) {
      for(var i=1; i<values.length; i++) {
        var keyval = values[i].split("=");
        if (keyval[0] == "serial") {
          returnValues["serial"] = keyval[1];
        }
      }
    }
    return returnValues;
  },
  generateDiskString: function(row) {
    var diskpathInput = row.querySelector("input[name=disk]");
    var diskSerialInput = row.querySelector("input[name=diskserial]");

    var formattedVal=diskpathInput.value;
    if (diskSerialInput.value.length > 0) {
      formattedVal=formattedVal+",serial="+diskSerialInput.value;
    }
    return formattedVal;
  }
});

var USBPassthroughSelectWidget = form.Value.extend({
  __name__: "USBPassthroughSelect",
  load: function(section_id) {
    listUSBDevices().then(this.loadConnectedDeviceList.bind(this));
    this.valueCells = [];
    this.newDropdowns = {};

    return this.super('load', section_id);
  },
  renderWidget: function(section_id, option_index, cfgvalue) {
    var sectionCBID = this.cbid(section_id);

    var tbody = E('tbody');
    var thead = E('thead', {}, [
      E('tr', {'class': 'tr cbi-section-table-titles'}, [
        E('th', {'class': 'th cbi-section-table-cell', 'colspan': '2'}, _('Device / Description')),
        E('th', {'class': 'th cbi-section-table-cell'}, _('Remove'))
      ]),
    ]);
    var table = E('table', {'class': 'table', 'id': sectionCBID}, [thead, tbody]);

    var addButton = E('button', {'class': 'cbi-button'}, _('Add another device'));
    addButton.onclick = L.ui.createHandlerFn(this, 'addAnotherDevice', sectionCBID);

    L.dom.bindClassInstance(table, this);
    this.tbody = tbody;

    if (Array.isArray(cfgvalue)) {
      for(var i=0; i<cfgvalue.length; i++) {
        this.addExistingDevice(cfgvalue[i]);
      }
    }

    return E([table, addButton]);
  },
  addExistingDevice: function(devName) {
    var deviceCell = E('td', {'class': 'td cbi-section-table-cell', 'data-value': devName}, devName);
    this.valueCells.push(deviceCell);
    var descriptionCell = E('td', {'class': 'td cbi-section-table-cell'});

    var removeButton = E('button', {'class': 'cbi-button'}, _('Remove'));
    removeButton.onclick = L.ui.createHandlerFn(this, 'removeExistingRow', deviceCell);

    var removeCell = E('td', {'class': 'td cbi-section-table-cell'}, removeButton);



    var row = E('tr', {}, [deviceCell, descriptionCell, removeCell]);

    this.tbody.appendChild(row);
  },
  addAnotherDevice: function(section_id, event) {
    var newIdx = this.tbody.childNodes.length;
    var dropdown = new ui.Dropdown("", this.formattedDevices, {
      id: section_id+"_"+newIdx,
      multiple: false,
      create: false
    });
    var deviceCell = E('td', {'colspan': '2', 'class': 'td cbi-section-table-cell'}, dropdown.render());
    this.newDropdowns[newIdx] = dropdown;

    var removeButton = E('button', {'class': 'cbi-button'}, _('Remove'));;
    removeButton.onclick = L.ui.createHandlerFn(this, 'removeNewRow', newIdx);
    var removeCell = E('td', {'class': 'td cbi-section-table-cell'}, removeButton);

    var deviceRow = E('tr',  {}, [deviceCell, removeCell]);
    this.tbody.appendChild(deviceRow);
  },
  removeExistingRow: function(deviceCell, event) {
    var parentRow = deviceCell.parentNode;
    this.tbody.removeChild(parentRow);

    this.valueCells.splice(
      this.valueCells.indexOf(deviceCell),
      1);
  },
  removeNewRow: function(idx, event) {
    delete this.newDropdowns[idx];
    var target = event.target;
    var parentRow = target.parentNode.parentNode;
    this.tbody.removeChild(parentRow);
  },
  textvalue: function(section_id) {
    return "";
  },
  formvalue: function(section_id) {
    var values = [];
    // Existing values
    for(var i=0; i<this.valueCells.length; i++) {
      var valueCell = this.valueCells[i];
      var value = valueCell.getAttribute("data-value");
      values.push(value);
    }
    // New values
    for(var dropDownKey in this.newDropdowns) {
      var dropdown = this.newDropdowns[dropDownKey];
      var valueKey = dropdown.getValue();
      var deviceInfo = this.connectedDevices[valueKey];
      var valueString = this.formatUSBString(valueKey, deviceInfo);
      values.push(valueString);
    }
    return values;
  },
  isValid: function(section_id) {
    var valid = true;
    for(var dropDownKey in this.newDropdowns) {
      var dropdown = this.newDropdowns[dropDownKey];
      var valueKey = dropdown.getValue();
      // Check that the device selected isn't being used (e.g by /dev/root)
      var deviceInfo = this.connectedDevices[valueKey];
      if ("existing_user" in deviceInfo) {
        valid = false;
      }
    }
    return valid;
  },
  formatUSBString: function(key, deviceInfo) {
    if ("serial" in deviceInfo) {
      return "serial="+deviceInfo["serial"];
    } else if ("idVendor" in deviceInfo && "idProduct" in deviceInfo) {
      return "vendor="+deviceInfo["idVendor"]+",product="+deviceInfo["idProduct"];
    } else {
      return "device="+key;
    }
  },
  loadConnectedDeviceList: function(devices) {
    this.connectedDevices = devices;
    var formattedDevices = {};
    for (var key in this.connectedDevices) {
      var deviceInfo = this.connectedDevices[key];

      var formatted = key + ": "+deviceInfo["manufacturer"];
      formatted += " " + deviceInfo["product"];
      if ("serial" in deviceInfo) {
         formatted += _(" S/N ") + deviceInfo["serial"];
      }
      if ("existing_user" in deviceInfo) {
        formatted = "(" + _("In use by ") + deviceInfo["existing_user"] + ") " + formatted;
      }
      formattedDevices[key] = formatted;
    }
    this.formattedDevices = formattedDevices;
  },
});

var StorageTypedSection = form.TypedSection.extend({
  __name__: "StorageSection",

  load: function() {
    this.storagestatus = {};

    var sections = this.cfgsections();
    sections.forEach((item, i) => {
      var processFunction = function(data) {
        this.loadStorageDeviceStatus(item,data)
      };

      getStorageOverview(item).then(
          processFunction.bind(this)
      );
    });
    return this.super('load');
  },

  renderContents: function(cfgsections, nodes) {
    console.log("StorageTypedSection renderContents");
    var section_id = null,
        config_name = this.uciconfig || this.map.config,
        sectionEl = E('div', {
        'id': 'cbi-%s-%s'.format(config_name, this.sectiontype),
        'class': 'cbi-section',
        'data-tab': (this.map.tabbed && !this.parentoption) ? this.sectiontype : null,
        'data-tab-title': (this.map.tabbed && !this.parentoption) ? this.title || this.sectiontype : null
      });

      //if (this.title != null && this.title != '')
      //  sectionEl.appendChild(E('legend', {}, this.title));
      sectionEl.appendChild(E('legend', {}, _('Storage status')));

      if (this.description != null && this.description != '')
        sectionEl.appendChild(E('div', { 'class': 'cbi-section-descr' }, this.description));
      cfgsections.forEach((item, i) => {
        if (this.storagestatus[item] != undefined) {
          var storagedevice = this.storagestatus[item];

          var devname = storagedevice["devname"];
          var driver = storagedevice["driver"];
          var total_mb = storagedevice["total_mb"] * (1024*1024);
          var free_mb = storagedevice["free_mb"] * (1024*1024);

          var sectionTitle = item;
          var driverSection = E('div', {'class': 'cbi-section-node'});
          driverSection.appendChild(E('h4', sectionTitle));
          var table = E('div',{'class':'table'});
          var driverRow = E('div', {'class': 'tr'},
            [
              E('div', { 'class': 'td left', 'width': '33%' }, _('Driver')),
              E('div', {'class': 'td left'}, driver),
            ]
          );
          var deviceRow = E('div', {'class': 'tr'},
            [
              E('div', { 'class': 'td left', 'width': '33%' }, _('Device name')),
              E('div', {'class': 'td left'}, devname),
            ]
          );

          var sizeRow = E('div', {'class': 'tr'},
            [
              E('div', { 'class': 'td left', 'width': '33%' }, _('Usage')),
              E('div', {'class': 'td left'}, progressbar(free_mb, total_mb, true)),
            ]
          );

          table.appendChild(driverRow);
          table.appendChild(deviceRow);
          table.appendChild(sizeRow);

          var volumesRow = E('div', {'class': 'tr'},
            [
              E('div', { 'colspan': 2, 'class': 'td' }, _('Volumes:')),
            ]
          );
          table.appendChild(volumesRow);

          if ("usage" in storagedevice) {
            var usage = storagedevice["usage"];
            for(var volname in usage) {
              var size = usage[volname] / 1024.0;
              var formatted = size.toFixed(2) + " GB";

              var volumeRow = E('div', {'class': 'tr'},
                [
                  E('div', { 'class': 'td left', 'width': '33%' }, volname),
                  E('div', { 'class': 'td left', 'width': '33%' }, formatted),
                ]
              );
              table.appendChild(volumeRow);
            }
          }

          driverSection.appendChild(table);


          sectionEl.appendChild(driverSection);
        }
      });

      L.dom.bindClassInstance(sectionEl, this);
      return sectionEl;
  },
  loadStorageDeviceStatus: function(device, data) {
    console.log("loadStorageDeviceStatus");
    this.storagestatus[device] = data;
  },
});

return L.Class.extend({
  OrderedNetworkSelect: OrderedNetworkSelectWidget,
  DiskSelect: DiskSelectWidget,
  USBPassthroughSelect: USBPassthroughSelectWidget,
  StorageSection: StorageTypedSection
});
