local lyaml = require "lyaml"
local uci = require"luci.model.uci".cursor()

local cconfig = {}

function cconfig._lookup_vm_config(vmname)
    local matchingVM = nil
    uci:foreach("virt", "vm", function(s)
        name = s[".name"]
        if (name == vmname) then matchingVM = s end
    end)
    return matchingVM
end

function cconfig.create_cconfig_for_table(vmtable)
    local cloudConfig = {}

    if (vmtable["userpw"] ~= nil) then
        cloudConfig["password"] = vmtable["userpw"]
    end
    if (vmtable["userpwexpire"] ~= nil and vmtable["userpwexpire"] == '1') then
        local chpasswdDict = {}
        chpasswdDict["expire"] = false
        cloudConfig["chpasswd"] = chpasswdDict
    end
    if (vmtable["sshkey"] ~= nil) then
        sshkeys = vmtable["sshkey"]
        ssh_authorized_keys = {}
        -- Try to tolerate SSH keys being specified
        -- as a list and as a bunch of strings
        if (type(sshkeys) == "table") then
            for k, v in pairs(sshkeys) do
                table.insert(ssh_authorized_keys, v)
            end
        else
            for line in string.gmatch(sshkeys, "[^\n]+") do
                table.insert(ssh_authorized_keys, line)
            end
        end
        cloudConfig["ssh_authorized_keys"] = ssh_authorized_keys
    end
    -- Set hostname to vmname if not specified
    if (vmtable["cchostname"] == nil) then
        cloudConfig["hostname"] = vmtable[".name"]
    elseif (vmtable["cchostname"] ~= nil) then
        cloudConfig["hostname"] = vmtable["cchostname"]
    end

    if (vmtable["cclocale"] ~= nil) then
        cloudConfig["locale"] = vmtable["cclocale"]
    end

    if (vmtable["script"] ~= nil) then
        -- TODO: take file and load here -> might need to be multipart?
    end

    return cloudConfig
end
function cconfig.create_cconfig_for_vm(vmname)
    local cloudConfig = {}

    local vmtable = cconfig._lookup_vm_config(vmname)
    if (vmtable == nil) then return nil end

    return cconfig.create_cconfig_for_table(vmtable)
end

function cconfig.set_first_time_poweroff(cloudConfig)
    -- Shutdown the VM after cloudinit has finished its work
    local power_state = {}
    power_state["mode"] = "poweroff"
    power_state["timeout"] = 30
    power_state["delay"] = "+2"
    power_state["condition"] = true

    cloudConfig["power_state"] = power_state
    return cloudConfig
end

function cconfig.serialize_cconfig(outfile, cloudConfig)
    local output = lyaml.dump({cloudConfig})
    local outputlen = output:len()
    local nowrapper = output:sub(5, outputlen - 5)
    outfile:write("#cloud-config\n")
    outfile:write(nowrapper)
    outfile:write("\n")
end

function cconfig.create_simple_network_config(dhcp_intf, dhcpv6)
    local do_dhcpv6 = dhcpv6 or false
    local networkconfigdoc = {}
    networkconfigdoc["version"] = 2

    local networkconfigethernets = {}

    local dhcp_intf_config = {}
    dhcp_intf_config["dhcp4"] = true
    if (do_dhcpv6 == true) then
        dhcp_intf_config["dhcp6"] = true
    end
    networkconfigethernets[dhcp_intf] = dhcp_intf_config

    networkconfigdoc["ethernets"] = networkconfigethernets

    return networkconfigdoc
end

return cconfig
