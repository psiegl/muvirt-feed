#
# Copyright (C) 2007-2014 OpenWrt.org
#
# This is free software, licensed under the GNU General Public License v2.
# See /LICENSE for more information.
#

include $(TOPDIR)/rules.mk

PKG_NAME:=muvirt
PKG_VERSION:=0.7.3
PKG_RELEASE:=1
PKG_INSTALL:=1

PKG_LICENSE:=GPL
PKG_MAINTAINER:=Mathew McBride <matt@traverse.com.au>

include $(INCLUDE_DIR)/package.mk

define Package/muvirt
	TITLE:=muvirt virtualization host
	URL:=http://www.traverse.com.au
	SECTION:=Network
	CATEGORY:=Traverse
	DEPENDS:=+qemu-aarch64-softmmu +kmod-tun +qemu-bridge-helper +uefi-aarch64 +tmux \
		+dosfstools +socat +maccalc +lua-md5 +luafilesystem +luaposix +coreutils +coreutils-stat \
		+muvirt-cloud-config +luci-base +@KERNEL_HUGETLB_PAGE +@KERNEL_PERF_EVENTS +@KERNEL_ARM_PMU \
		+kmod-loop +procd-ujail +nsenter +parted +qemu-img +lua-hashutil
	USERID:=muvirt-console=1123:muvirt-console=1123
endef

define Package/muvirt/description
	Provides virtualization facilities on LEDE, allowing VMs to be defined in UCI,
	and for the VM network to be bridged to a UCI-defined bridge.
endef

define Package/muvirt/conffiles
/etc/config/virt
endef

define Build/Install
endef

define Build/Prepare
	mkdir -p $(PKG_BUILD_DIR)
endef

define Build/Compile/Default

endef
Build/Compile = $(Build/Compile/Default)

define Package/muvirt/install
	$(INSTALL_DIR)	$(1)/etc/init.d/
	$(INSTALL_BIN) 	files/muvirt.init $(1)/etc/init.d/muvirt
	$(INSTALL_DIR)	$(1)/usr/sbin/
	$(INSTALL_BIN)	files/muvirt-console $(1)/usr/sbin/muvirt-console
	$(INSTALL_BIN)	files/muvirt-command $(1)/usr/sbin/muvirt-command
	$(INSTALL_BIN)  files/muvirt-provision $(1)/usr/sbin/muvirt-provision
	$(INSTALL_BIN)	files/muvirt-createvm $(1)/usr/sbin/muvirt-createvm
	$(INSTALL_BIN)	files/muvirt-status	$(1)/usr/sbin/muvirt-status
	$(INSTALL_BIN)	files/muvirt-system-setup $(1)/usr/sbin/muvirt-system-setup

	$(INSTALL_DIR)	$(1)/etc/hotplug.d/usb/
	$(INSTALL_BIN)	files/muvirt-usb-hotplug-handler $(1)/etc/hotplug.d/usb/10-muvirt
	$(INSTALL_DIR)	$(1)/etc/config/
	$(INSTALL_DIR)	$(1)/lib/muvirt/
	$(INSTALL_BIN)	files/lib/muvirt/muvirt-guest-info $(1)/lib/muvirt/muvirt-guest-info
	$(INSTALL_BIN)	files/lib/muvirt/muvirt-guest-info-web $(1)/lib/muvirt/muvirt-guest-info-web
	$(INSTALL_BIN)	files/lib/muvirt/muvirt-qga-client $(1)/lib/muvirt/muvirt-qga-client
	$(INSTALL_BIN)  files/lib/muvirt/muvirt-usb-hotplug $(1)/lib/muvirt/muvirt-usb-hotplug
	$(INSTALL_BIN)	files/lib/muvirt/initfuncs.sh $(1)/lib/muvirt/
	$(INSTALL_BIN)	files/lib/muvirt/provfuncs.sh $(1)/lib/muvirt/provfuncs.sh

	$(INSTALL_DIR) 	$(1)/lib/muvirt/usb/
	$(INSTALL_BIN)	files/lib/muvirt/usb/find_usb_devices.sh $(1)/lib/muvirt/usb/
	$(INSTALL_CONF) files/muvirt.config $(1)/etc/config/virt

	$(INSTALL_DIR)	$(1)/usr/libexec/muvirt

	$(CP) -r files/libexec/storage $(1)/usr/libexec/muvirt/
	$(CP) -r files/libexec/prov $(1)/usr/libexec/muvirt/

	$(INSTALL_BIN)	files/libexec/create-consolejail $(1)/usr/libexec/muvirt/
	$(INSTALL_BIN)	files/libexec/console-jailshell $(1)/usr/libexec/muvirt/
	$(INSTALL_BIN)	files/libexec/console-pause $(1)/usr/libexec/muvirt/

	$(INSTALL_DIR)	$(1)/usr/libexec/muvirt/provisioners/
	$(INSTALL_BIN)	files/libexec/generic-provisioner $(1)/usr/libexec/muvirt/provisioners/generic

	$(INSTALL_DIR)	$(1)/usr/lib/lua
	$(INSTALL_BIN)	files/muvirt.lua $(1)/usr/lib/lua
	$(INSTALL_BIN)	files/qemuga.lua $(1)/usr/lib/lua
	$(INSTALL_BIN)	files/muvirtsetup.lua $(1)/usr/lib/lua

	$(INSTALL_DIR)	$(1)/usr/libexec/rpcd
	$(INSTALL_BIN) 	files/libexec/rpcd/muvirtstorage $(1)/usr/libexec/rpcd/
	$(INSTALL_BIN)	files/libexec/rpcd/muvirt $(1)/usr/libexec/rpcd/

	$(INSTALL_DIR)	$(1)/etc/uci-defaults/
	$(INSTALL_BIN) 	files/uci-defaults/muvirt $(1)/etc/uci-defaults/35-muvirt
endef


$(eval $(call BuildPackage,muvirt))
