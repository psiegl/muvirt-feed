require 'pl'
local jsonc = require 'luci.jsonc'
local uci = require "luci.model.uci".cursor()

local muvirtsetup = {}

function muvirtsetup.expand_gpt(device)
    local expand_command = "printf \"fix\\n\" | parted ---pretend-input-tty "..device.." print"
    local expand_success, expand_return, expand_stdout, expand_stderr = utils.executeex(expand_command, false)
    return expand_success
end

function muvirtsetup.run_parted(device, command)
    local parted_command = string.format("parted -s -a optimal -- %s %s", device, utils.quote_arg(command))
    local parted_success, parted_return, parted_stdout, parted_stderr  = utils.executeex(parted_command, false)
    if (parted_success ~= true) then
        error("Unable to run command \""..parted_command.."\":" .. parted_stderr)
    end
end

function muvirtsetup.fix_residual_gpts(device)
    local remove_end_disk_residual_command = "printf \"OK\\nfix\\n\" | parted ---pretend-input-tty " .. device .. " print 2>/dev/null 1>/dev/null"
    utils.executeex(remove_end_disk_residual_command)
end

function muvirtsetup.get_partition_dev_for_device(device, part_idx)
    lastchar = device:sub(-1)
    if (tonumber(lastchar) == nil) then
        return device .. part_idx
    end
    return device .. "p" .. part_idx
end

function muvirtsetup.get_possible_block_devices()
    lsblk_success, lsblk_retcode, lsblk_stdout, lsblk_stderr =
        utils.executeex("lsblk -o NAME,MODEL,SERIAL,SIZE,TYPE,MOUNTPOINT -J -b")
    if (lsblk_success ~= true) then
        error("Could not scrape output from lsblk, stderr: " .. lsblk_stderr)
    end
    local lsblk_table = jsonc.parse(lsblk_stdout)
    if (lsblk_table["blockdevices"] == nil) then
        error("No \"blockdevices\" in lsblk output")
    end

    local block_devices = {}

    local lsblk_devices = lsblk_table["blockdevices"]
    for k,v in pairs(lsblk_devices) do
        local name = v["name"]
        local devtype = v["type"]
        if (devtype == "disk") then
            local model = v["model"]
            if (model ~= nil) then
                model = stringx.rstrip(model)
            else
                model = ""
            end

            local serial = v["serial"]
            if (serial == nil) then
                serial = ""
            end
            local size = v["size"] / (1024*1024*1024)
            local block_device_data = {}
            block_device_data["model"] = model
            block_device_data["serial"] = serial
            block_device_data["size"] = size

            local device_children = v["children"]
            if (device_children ~= nil) then
                for child_idx, child_node in pairs(device_children) do
                    if (child_node["mountpoint"] == "/") then
                        block_device_data["rootdevice"] = true
                    end
                end
            end

            block_devices[name] = block_device_data
        end
    end
    return block_devices
end

function muvirtsetup.get_last_partition_end(device)
    local parted_get_success, parted_get_return,
        parted_get_stdout, parted_get_stderr = utils.executeex("parted -s -m " .. device .. " 'print'")
    if (parted_get_success ~= true) then
        error("Could not execute parted print on " .. device .. " : " .. parted_get_stderr)
    end
    local last_delim = stringx.rfind(parted_get_stdout,";")
    local last_line_seperator = stringx.rfind(parted_get_stdout, "\n",0, last_delim)
    local last_line = parted_get_stdout:sub(last_line_seperator)
    local last_line_tokens = stringx.split(last_line, ":")
    local last_end_mb_str = last_line_tokens[3]
    for token in last_end_mb_str:gmatch("(%d+)") do
        return tonumber(token)
    end
    return -1
end

function muvirtsetup.find_part_with_partlabel(partlabel)
    local blkid_success, blkid_retcode, blkid_stdout, blkid_stderr =
        utils.executeex("blkid --match-token 'PARTLABEL="..partlabel.."' -o device")
    if (blkid_success ~= true) then
        error("Unable to get device for " .. partlabel)
    end
    local stripped_output = stringx.rstrip(blkid_stdout)
    return stripped_output
end

function muvirtsetup.create_lvm_vg(devpath)
    -- Erase previous data so vgcreate does not complain
    utils.executeex("dd if=/dev/zero of="..utils.quote_arg(devpath).." bs=1024 count=5192")
    local vgcreate_success, vgcreate_retcode, vgcreate_stdout, vgcreate_stderr =
        utils.executeex("vgcreate vmdata " .. utils.quote_arg(devpath))
    if (vgcreate_success ~= true) then
        error("Could not create LVM VG on " .. devpath .. " : " ..vgcreate_stderr)
    end
end

function muvirtsetup.create_worklv(worklvsize)
    
    local lvcreate_success, lvcreate_retcode, lvcreate_stdout,
        lvcreate_stderr = utils.executeex("lvcreate --name muvirtwork --size " .. worklvsize .. "MiB vmdata")
    if (lvcreate_success ~= true) then
        error("Could not create a LVM LV for muvirtwork: " .. lvcreate_stderr)
    end
end

function muvirtsetup.partition_disk(device, mode, swapsize, worksize)
    if (mode == "fulldisk") then
        muvirtsetup.run_parted(device, "mklabel gpt")
    else
        muvirtsetup.fix_residual_gpts(device)
        muvirtsetup.expand_gpt(device)
    end

    local swapg = swapsize / 1024
    local vmdata_start = "0"
    local swap_start = "-"..swapg.."GiB"
    if (mode ~= "fulldisk") then
        local last_mb = muvirtsetup.get_last_partition_end(device)
        vmdata_start = last_mb + 1
    end
    muvirtsetup.run_parted(device, "mkpart vmdata " .. vmdata_start .. "MB " .. swap_start)
    muvirtsetup.run_parted(device, "mkpart swap " .. swap_start .. " 100%")
end

function muvirtsetup.format_ext4(devpath)
    local format_success, format_retcode, format_stdout, format_stderr =
        utils.executeex("mkfs.ext4 " .. utils.quote_arg(devpath))
end

function muvirtsetup.add_owrt_fstab_mount(devpath, targetdir, name)
    local newfstab = {}
    newfstab["device"] = devpath
    newfstab["target"] = targetdir
    newfstab["enabled"] = "1"
    local saved = uci:section("fstab","mount",name, newfstab)
    uci:save("fstab")
    uci.apply()
end

function muvirtsetup.add_owrt_swap_mount(devpath)
    local newswap = {}
    newswap["device"] = devpath
    newswap["enabled"] = "1"
    local saved = uci:section("fstab","swap", nil, newswap)
    uci:save("fstab")
    uci.apply()
end

function muvirtsetup.create_swap_partition(partdevice)
    local mkswap_success, mkswap_return, mkswap_stdout, mkswap_stderr = utils.executeex("mkswap "..partdevice)
    if (mkswap_success ~= true) then
        error("Could not format a swap partition on " .. partdevice .. ": " .. mkswap_stderr)
    end

end

function muvirtsetup.set_muvirt_storage_driver(driver, device)
    local vmdata_storage = {}
    vmdata_storage["driver"] = driver
    vmdata_storage["device"] = device

    local saved = uci:section("virt","storage","vmdata",vmdata_storage)
    uci:save("virt")
    uci.apply()
end

function muvirtsetup.set_workdir(path)
    local sectionname = nil
    local sectiondata = {}
    uci:foreach("virt","muvirt", function(s)
        if (sectionname == nil) then
            sectionname = s[".name"]
            sectiondata = s
        end
    end
    )
    uci:set("virt",sectionname, "scratch", path)
    uci:save("virt")
    uci.apply()
end

function muvirtsetup.set_appstore_workdir(path)
    local sectionname = nil
    local sectiondata = {}
    uci:foreach("appstore","appstore", function(s)
        if (sectionname == nil) then
            sectionname = s[".name"]
            sectiondata = s
        end
    end
    )
    uci:set("appstore",sectionname, "listcache", path)
    uci:save("virt")
    uci.apply()
end

function muvirtsetup.is_setup_already()
    local has_storage = false
    uci:foreach("virt","storage", function(s)
        has_storage = true
    end
    )
    return has_storage
end

function muvirtsetup.do_setup(device, mode, swapsize, worksize)

    muvirtsetup.partition_disk(device, mode, swapsize, worksize)
    local vmdata_part = muvirtsetup.find_part_with_partlabel("vmdata")
    
    muvirtsetup.create_lvm_vg(vmdata_part)
    muvirtsetup.create_worklv(worksize)
    muvirtsetup.format_ext4("/dev/mapper/vmdata-muvirtwork")
    muvirtsetup.add_owrt_fstab_mount("/dev/mapper/vmdata-muvirtwork","/mnt/muvirtwork","muvirtwork")

    muvirtsetup.set_workdir("/mnt/muvirtwork")
    muvirtsetup.set_appstore_workdir("/mnt/muvirtwork/appstore/")

    local swap_part = muvirtsetup.find_part_with_partlabel("swap")
    muvirtsetup.create_swap_partition(swap_part)
    muvirtsetup.add_owrt_swap_mount(swap_part)

    utils.executeex("block mount")

    muvirtsetup.set_muvirt_storage_driver("lvm","vmdata")

    utils.executeex("swapon " .. swap_part)
end

return muvirtsetup