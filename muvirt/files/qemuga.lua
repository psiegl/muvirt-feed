#!/usr/bin/lua

local plfile = require 'pl.file'
local plutil = require 'pl.utils'

local jsonc = require 'luci.jsonc'
local nixio = require 'nixio'

local app = require 'pl.app'

local qemuga = {}

function qemuga.create_command(command_name, arguments)
    local command = {}
    command["execute"] = command_name
    command["arguments"] = arguments
    return command
end

function qemuga.create_open_command(fpath)
    local node_token_args = {}
    node_token_args["path"] = fpath
    node_token_args["mode"] = "r"

    local fullcommand =
        qemuga.create_command("guest-file-open", node_token_args)
    return fullcommand
end

function qemuga.create_read_command(handle, size)
    local readargs = {}
    readargs["handle"] = handle
    readargs["count"] = size

    local fullcommand = qemuga.create_command("guest-file-read", readargs)
    return fullcommand
end

function qemuga.create_close_command(handle)
    local closeargs = {}
    closeargs["handle"] = handle

    local fullcommand = qemuga.create_command("guest-file-close", closeargs)
    return fullcommand
end

function qemuga.getcommandresponse(vmname, command_table)
    command_json = jsonc.stringify(command_table)
    plfile.write("/tmp/.qga-command-in", command_json)

    socket_file = string.format("unix-connect:/tmp/qemu-qga-%s", vmname)
    plutil.executeex(
        "socat system:\"/lib/muvirt/muvirt-qga-client /tmp/.qga-command-in /tmp/.qga-command-out\" " ..
            socket_file)

    output = plfile.read("/tmp/.qga-command-out")
    if (output == nil) then return nil end
    response = jsonc.parse(output)

    plfile.delete("/tmp/.qga-command-in")
    plfile.delete("/tmp/.qga-command-out")
    return response
end

function qemuga.decode_file_read(file_read_response)
    local b64buf = file_read_response["buf-b64"]
    local decoded = nixio.bin.b64decode(b64buf)
    return decoded
end

function qemuga.read_vm_file(vmname, filepath)
    if (vmname == nil) then error("nil vmname specified") end
    if (filepath == nil) then error("no filepath specified") end

    create_open_command_json = qemuga.create_open_command(filepath)

    open_handle_response = qemuga.getcommandresponse(vmname,
                                                     create_open_command_json)
    if (open_handle_response == nil) then 
        return nil 
    end
    
    handle = open_handle_response["return"]
    if (handle == nil) then
        return nil 
    end

    readcommand = qemuga.create_read_command(handle, 512)

    read_command_response = qemuga.getcommandresponse(vmname, readcommand)
    read_response = read_command_response["return"]

    token = qemuga.decode_file_read(read_response)

    closecommand = qemuga.create_close_command(handle)
    close_command_response = qemuga.getcommandresponse(vmname, closecommand)

    return token
end

function qemuga.get_network_interfaces(vmname)
    local network_interfaces_command = qemuga.create_command(
                                           "guest-network-get-interfaces", nil)

    network_interfaces_response = qemuga.getcommandresponse(vmname,
                                                            network_interfaces_command)
    return network_interfaces_response
end

function qemuga.get_interface_ip(vmname, interface, addrfamily)
    local family = "ipv4"

    if (addrfamily ~= nil) then
        family = addrfamily
    end

    local network_interfaces = qemuga.get_network_interfaces(vmname)
    if (network_interfaces == nil) or (network_interfaces["return"] == nil) then
        error("Could not get network interface data for vm " .. vmname)
    end

    local return_data = network_interfaces["return"]
    for k, v in pairs(return_data) do
        if (v["name"] == interface) then
            local ipaddrs = v["ip-addresses"]
            for x, y in pairs(ipaddrs) do
                local ipaddr = y["ip-address"]
                local iptype = y["ip-address-type"]
                if (iptype == family) then
                    return ipaddr
                end
            end
        end
    end
    return nil
end

return qemuga
