local uci = require "luci.model.uci".cursor()
local util = require "luci.util"

local muvirt = {}

function muvirt_get_instances()
	local knownvms = {}
	local instances = util.ubus("service","list", {name = "muvirt"})
	local configs = uci:foreach("virt","vm", function(s)
		local name = s[".name"]
		properties = {}
		properties["enabled"] = (s.enable == "1" and true or false)
		properties["provisioned"] = (s.provisioned == "1" and true or false)
		properties["numprocs"] = (s.numprocs and tonumber(s.numprocs) or 1)
		properties["mem"] = (s.memory and tonumber(s.memory) or 0)
		if (s["_downloading"] ~= nil and s["_downloading"] == "1") then
			properties["status"] = "downloading"
		elseif (s["_downloading"] ~= nil and s["_downloading"] == "-1") then
			properties["status"] = "download-failed"
		elseif (s["_configuring"] ~= nil and s["_configuring"] == "1") then
			properties["status"] = "configuring"
		elseif (s["_configuring"] ~= nil and s["_configuring"] == "-1") then
			properties["status"] = "configure-failed"
		elseif (s.provisioned == "0" and true or false) then
			properties["status"] = "to-be-provisioned"
		elseif (s.enable == "0" and true or false) then
			properties["status"] = "disabled"
		else
			properties["status"] = "off"
		end
		properties["running"] = false
		knownvms[name] = properties
	end
	)
	if instances.muvirt ~= nil and instances.muvirt.instances ~= nil then
		allinstances = instances["muvirt"]["instances"]
		for vmname, vmprops in pairs(allinstances) do
			running = vmprops["running"]
			-- Make sure the VM is in the current UCI state - if deleted but
			-- previously running it might not be.
			if knownvms[vmname] ~= nil then
				knownvms[vmname]["running"] = running
				if running == true then
					if (knownvms[vmname]["status"] ~= "configuring") then
						knownvms[vmname]["status"] = "on"
					end
					knownvms[vmname]["pid"] = vmprops["pid"]
					local net_info_pipe = io.popen("/lib/muvirt/muvirt-guest-info-web %s" % luci.util.shellquote(vmname))
					local net_info = net_info_pipe:read("*a")
					knownvms[vmname]["network"] = net_info
					net_info_pipe:close()
				elseif (vmprops["exit_code"] ~= nil and vmprops["exit_code"] > 0) then
					exitcode = vmprops["exit_code"]
					knownvms[vmname]["status"] = "start-failed"
				end
			end
		end
	end
  return knownvms
end

function muvirt_generate_random_mac()
  math.randomseed(os.time())
  local firstOctet = math.random(0,255)
  local secondOctet = math.random(0,255)
  local thirdOctet = math.random(0,255)
  local formattedMac = string.format("52:54:00:%02X:%02X:%02X", firstOctet, secondOctet, thirdOctet)
  return formattedMac
end

function muvirt_get_default_network()
  defaultnet = nil
  uci:foreach("virt","muvirt", function(s)
      name = s[".name"]
      if (s["defaultnet"] ~= nil and defaultnet == nil) then
        defaultnet = s["defaultnet"]
      end
    end)
  if (defaultnet == nil) then
    defaultnet = "lan"
  end
  return defaultnet
end

function muvirt.is_device_bridge(devicename)
	local is_bridge=false
	configs = uci:foreach("network","device",
		function(s)
			name = s["name"]
			type = s["type"]
			if (devicename == name) then
				if (type == "bridge") then
					is_bridge=true
				end
			end
		end
	)
	return is_bridge
end

function muvirt.get_workdir()
	workdir = nil
	uci:foreach("virt","muvirt", function(s)
		name = s[".name"]
		if (s["scratch"] ~= nil and workdir == nil) then
			workdir = s["scratch"]
		end
	  end)
	if (workdir == nil) then
		workdir = "lan"
	end
	return workdir
end

function muvirt.get_library_dir()
	local workdir = muvirt.get_workdir()
	return workdir .. "/imglibrary"
end

return muvirt