#!/bin/sh
# shellcheck disable=SC2039

# This acts as a proxy to the defined storage driver
. "/lib/functions.sh"

(uci -q get virt.@storage[0] > /dev/null) || (echo "ERROR: No storage device defined" && exit 255)

STORAGE_DRIVER=$(uci -q get "virt.@storage[0].driver")
STORAGE_DEVICE=$(uci -q get "virt.@storage[0].device")

DRIVER_PATH="/usr/libexec/muvirt/storage/${STORAGE_DRIVER}"
if [ ! -f "${DRIVER_PATH}" ]; then
    echo "ERROR: ${DRIVER_PATH} not found"
    exit 255
fi

. "${DRIVER_PATH}" "${STORAGE_DEVICE}" $*
