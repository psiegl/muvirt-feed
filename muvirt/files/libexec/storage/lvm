#!/bin/sh
# shellcheck disable=SC2039

# LVM storage driver
. "/usr/share/libubox/jshn.sh"

new_volume() {
  device="${1}"
  volname="${2}"
  size="${3}"

  # Check for existing
  if [ -b "/dev/mapper/${device}-${volname}" ]; then
    echo "ERROR: volume ${volname} already exists in ${device}!"
    exit 255
  fi

  lvcreate -y -Z y -W y --name "${volname}" --size "${size}" "${device}" 2>/dev/null > /dev/null
  if [ ! "$?" -eq 0 ]; then
    echo "ERROR: Could not create volume"
    exit 255
  fi
  echo -n "/dev/mapper/${device}-${volname}"
}

resize_volume() {
  true;
}

remove_volume() {
  local device="${1}"
  local volname="${2}"
  if [ ! -b "/dev/mapper/${device}-${volname}" ]; then
    echo "ERROR: volume ${volname} does not exist in ${device}!"
    exit 255
  fi
  # Zero out the start of the volume to prevent warnings if a new
  # partition is established from the same offset
  dd if=/dev/zero of="/dev/mapper/${device}-${volname}" bs=512 count=1400
  lvremove -y "${device}/${volname}"
}

init_device() {
  true;
}

list_volume_usage() {
  devname="${1}"
  volume_names=$(lvs --no-heading --no-suffix --units m -o lv_name "${devname}")
  json_init
  for vol in ${volume_names}; do
    volume_size=$(lvs --no-heading --no-suffix --units m -o lv_size "${devname}/${vol}")
    json_add_double "${vol}" "${volume_size}"
  done
  json_dump
}

get_free() {
  devname="${1}"
  freedata=$(vgs --no-heading --no-suffix --units m -o vg_free,vg_size "${devname}")
  free_percent=$(echo "${freedata}" | awk '{print $1/$2 * 100.0}')
  free_mb=$(echo "${freedata}" | awk '{print $1}')
  total_mb=$(echo "${freedata}" | awk '{print $2}')

  json_init
  json_add_string "devname" "${devname}"
  json_add_string "driver" "lvm"
  json_add_int "free_mb" "${free_mb}"
  json_add_int "total_mb" "${total_mb}"
  json_add_double "free_percent" "${free_percent}"
  json_dump
}

get_volume_path() {
  local devname="${1}"
  local volname="${2}"
  if [ ! -b "/dev/mapper/${devname}-${volname}" ]; then
      echo "ERROR: ${volname} not find in ${devname}"
      exit 255
  fi
  echo -n "/dev/mapper/${devname}-${volname}"
}

write_qcow2_image() {
  local devname="${1}"
  local volname="${2}"
  local image="${3}"

  volume_path=$(get_volume_path "${devname}" "${volname}")  || exit 255

  qemu-img convert -O raw "${image}" "${volume_path}"
}

ACTION="${2}"
case "${ACTION}" in
    new_volume)
      [ "$#" -lt 4 ] && echo "Error: get_free requires one argument: storage device" && exit 255
      new_volume "${1}" "${3}" "${4}"
      ;;

    resize_volume)
      resize_volume "${1}" "${3}" "${4}"
      ;;

    remove_volume)
      remove_volume "${1}" "${3}"
      ;;

    init_device)
      init_device "${1}"
      ;;

    list_volume_usage)
      [ "$#" -lt 2 ] && echo "Error: list_volume_usage requires one argument: storage device" && exit 255
      list_volume_usage "${1}"
      ;;

    get_free)
      [ "$#" -lt 2 ] && echo "Error: get_free requires one argument: storage device" && exit 255
      get_free "${1}"
      ;;

    get_volume_path)
      [ "$#" -lt 2 ] && echo "Error: get_volume_path requires one argument: volume path" && exit 255
      get_volume_path "${1}" "${3}"
      ;;

    write_qcow2_image)
      [ "$#" -lt 2 ] && echo "Error: write_qcow2_image requires two arguments: volume and qcow2 image" && exit 255
      write_qcow2_image "${1}" "${3}" "${4}"
      ;;

    *)
      echo "Unknown action ${2}"
      echo "Valid actions are: "
      printf "\t new_volume <name> <size>\n"
      printf "\t get_volume_path <name>\n"
      printf "\t resize_volume <name> <newsize>\n"
      printf "\t remove_volume <name>\n"
      printf "\t init_device </dev/something>\n"
      printf "\t get_free <device>\n"
      ;;
esac
